import { StatusBar } from "expo-status-bar";
import { StyleSheet, ScrollView, Text, View } from "react-native";
import { Appbar } from "./components/app_bar";
import { MainItem } from "./components/main_item";
import { Items } from "./components/grid_items";
import CarouselCards from "./components/carousel_cards";
import { useState } from "react";

export default function App() {
  const [label, setLabel] = useState();

  return (
    <View style={styles.container}>
      <Appbar setLabelCallback={setLabel}/>
      <ScrollView
        style={styles.scrollStyle}
        contentContainerStyle={{
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <View style={{ width: "100%", flex: 1 }}>
          <CarouselCards />
        </View>
        <View style={{ width: "100%", flex: 3, alignItems: "center" }}>
          <MainItem />
          <Items label={label} />
        </View>
        <View style={{ height: 20 }} />
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  scrollStyle: {
    width: "100%",
  },
  container: {
    flex: 1,
    backgroundColor: "#fffffd",
    alignItems: "center",
    width: "100%",
  },
});
