import React from "react";
import { View } from "react-native";
import Carousel, { Pagination } from "react-native-snap-carousel";
import CarouselCardItem, {
  SLIDER_WIDTH,
  ITEM_WIDTH,
} from "./carousel_car_item";
import data from "../data/data";

const CarouselCards = () => {
  const isCarousel = React.useRef(null);
  const [index, setIndex] = React.useState(0);

  return (
    <View >
      <Carousel
        layout={"default"}
        layoutCardOffset={9}
        ref={isCarousel}
        data={data}
        renderItem={CarouselCardItem}
        sliderWidth={SLIDER_WIDTH}
        itemWidth={ITEM_WIDTH}
        inactiveSlideShift={0}
        useScrollView={true}
        onSnapToItem={(index) => setIndex(index)}
      />
      <Pagination
        dotsLength={data.length}
        activeDotIndex={index}
        carouselRef={isCarousel}
        
        containerStyle={{
          // height: 70,
          paddingVertical: 7,
        }}
        dotStyle={{
          width: 8,
          height: 8,
          borderRadius: 5,
          margin:0,
          backgroundColor: "#165B78",
          
        }}
        inactiveDotStyle={{
          backgroundColor: "#8e8e8e"
        }}
        inactiveDotOpacity={1}
        inactiveDotScale={1}
        tappableDots={true}
      />
    </View>
  );
};

export default CarouselCards;
