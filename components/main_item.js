import React from "react";
import { Image, StyleSheet, Text, View } from "react-native";

import Icon from "react-native-vector-icons/FontAwesome";

export const MainItem = () => (
  <View style={styles.container}>
      {/* <Image
        sources={require("../assets/image2.jpg")}
        style={{ width: 100, height: 70 ,backgroundColor: "white" }}
      /> */}
        <Icon name="cubes" size={66} style={styles.iconStyle} />
      <Text style={styles.textStyle}>Products List</Text>
  </View>
);

const styles = StyleSheet.create({
  iconStyle: {
    color: "white",
  },
  textStyle: {
    paddingTop: 10,
    color: "#165B78",
    fontWeight: "bold",
    fontSize: 24,
  },
  container: {
    backgroundColor: "rgba(46, 181, 236, 0.6)",
    borderRadius: 100,
    alignItems: "center",
    justifyContent: "center",
    padding: 10,
    height: 150,
    width: "92%",
  },
});
