import React from "react";
import {
  TouchableNativeFeedback,
  Image,
  StyleSheet,
  Text,
  View,
} from "react-native";
import CarouselCards from "./carousel_cards";
import Icon from "react-native-vector-icons/FontAwesome";

export const Items = ({ label }) => (
  <View style={{ alignItems: "center", justifyContent: "center" }}>
    <View
      style={{ flexDirection: "row", marginHorizontal: "2%", paddingTop: 7 }}
    >
      <View style={[styles.container, styles.leftRadius, styles.greyColor]}>
        <TouchableNativeFeedback
          background={TouchableNativeFeedback.Ripple("#165B78", true, 2100)}
          onPress={() => {}}
        >
          <View style={{ alignItems: "center" }}>
            <Icon name="drupal" size={50} style={styles.iconStyle} />
            <Text style={styles.textStyle}>
              {label === undefined || label.length == 0
                ? "Customer Care"
                : label}
            </Text>
            <View style={{ paddingTop: 10 }} />
          </View>
        </TouchableNativeFeedback>
      </View>
      <View style={{ paddingHorizontal: 4 }}></View>
      <View style={[styles.container, styles.rightRadius, styles.blueColor]}>
        <Icon name="joomla" size={50} style={styles.iconStyle} />
        <Text style={styles.textStyle}>Stores Locations</Text>

        <View style={{ paddingTop: 10 }} />
      </View>
    </View>
    <View
      style={{ flexDirection: "row", marginHorizontal: "2%", paddingTop: 13 }}
    >
      <View style={[styles.container, styles.leftRadius, styles.blueColor]}>
        <Icon name="soundcloud" size={50} style={styles.iconStyle} />
        <Text style={styles.textStyle}>Online shopping</Text>
      </View>
      <View style={{ paddingHorizontal: 4 }}></View>
      <View style={[styles.container, styles.rightRadius, styles.greyColor]}>
        <Icon name="tree" size={50} style={styles.iconStyle} />
        <Text style={styles.textStyle}>New Products</Text>
      </View>
    </View>
    <View style={styles.outCircleContainer}>
      <View style={styles.circleContainer}>
        <Icon name="empire" size={40} color={"white"} />
      </View>
    </View>
  </View>
);

const styles = StyleSheet.create({
  iconStyle: {
    color: "white",
  },
  textStyle: {
    paddingTop: 10,
    color: "#165B78",
    fontWeight: "bold",
    fontSize: 18,
  },
  rightRadius: {
    borderTopEndRadius: 90,
    borderBottomEndRadius: 90,
  },
  blueColor: {
    backgroundColor: "rgba(46, 181, 236, 0.6)",
  },
  greyColor: {
    backgroundColor: "#CDCDCD",
  },
  leftRadius: {
    borderTopStartRadius: 90,
    borderBottomStartRadius: 90,
  },
  container: {
    alignItems: "center",
    justifyContent: "center",
    padding: 10,
    height: 150,
    width: "47%",
  },
  circleContainer: {
    position: "absolute",
    justifyContent: "center",

    alignItems: "center",
    backgroundColor: "rgb(46, 181, 236)",
    borderRadius: 180,
    height: 70,
    width: 70,
  },
  outCircleContainer: {
    position: "absolute",
    alignItems: "center",
    backgroundColor: "white",
    padding: 42,
    borderRadius: 179,
    justifyContent: "center",
  },
});
