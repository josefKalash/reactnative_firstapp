import { TextInput, StyleSheet, Text, View } from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";
import Icon2 from "react-native-vector-icons/Ionicons";
import Icon3 from "react-native-vector-icons/AntDesign";

export const Appbar = ({ setLabelCallback }) => {
  return (
    <View style={[styles.barStyle, styles.shadowProp]}>
      <Icon name="navicon" size={30} color={"rgb(46, 181, 236)"} />
      <View style={{ width: 10 }} />
      <Icon2 name="notifications" size={30} color={"rgb(46, 181, 236)"} />
      <View style={{ width: 10 }} />
      <Icon3 name="clockcircle" size={30} color={"red"} />
      <View style={styles.textInput}>
        <Icon3 name="search1" size={20} color={"#8e8e8e"} />
        <TextInput
          placeholder="search..."
          placeholderTextColor={"#8e8e8e"}
          style={{ flex: 2, paddingHorizontal: 2 }}
          paddin={0}
          margin={0}
          onChangeText={(text) => setLabelCallback(text)}
          underlineColor={"transparent"}
          dense={true}
        />
        <Icon name="mobile" size={26} color={"#165B78"} />
        <Icon2
          name="options"
          size={26}
          color={"#165B78"}
          style={{ transform: [{ rotate: "90deg" }] }}
        />
      </View>
      <Icon name="home" size={30} color={"grey"} />

      <View style={{ width: 10 }} />
    </View>
  );
};

const styles = StyleSheet.create({
  textInput: {
    flex: 1,
    borderRadius: 18,
    backgroundColor: "#E0E0E0",
    paddingVertical: 4,
    flexDirection: "row",
    alignItems: "center",
    // height: 40,
    justifyContent: "flex-start",
    color: "#8E9e9e",
    paddingHorizontal: 10,
    marginHorizontal: 20,
  },
  barStyle: {
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 10 },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 20,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    backgroundColor: "white",
    display: "flex",
    alignItems: "center",
    width: "100%",
    flexDirection: "row",
    textAlign: "center",
    paddingHorizontal: 20,
    paddingTop: 40,
    padding: 15,
  },
});
